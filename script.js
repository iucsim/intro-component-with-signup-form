const form = document.querySelector("form");
const inputs = document.querySelectorAll("form input");

form.addEventListener('submit', (e) => {
    e.preventDefault();

    Array.from(inputs).forEach(function (input) {
        const inputVal = input.value;
        const errTxt = input.parentElement.querySelector('small');

        if (!inputVal) {
            input.classList.add('error');
            errTxt.style.display = "inline-block";
        } else {
            input.classList.remove('error');
            errTxt.style.display = "none";
            if (input.type == email) {
                if (isValidEmail(inputVal)) {
                    input.classList.remove('error');
                    errTxt.style.display = "none";
                } else {
                    input.classList.add('error');
                    errTxt.style.display = "inline-block";
                }
            }
        }

    })

})
function isValidEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}